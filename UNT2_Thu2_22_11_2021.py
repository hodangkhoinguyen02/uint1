#Bài 1
"""
Những hàm dạng có sẵn : print(),int(), str(), float(),.. Đây là những hàm người ta đã xây dựng sẵn cho mình
Mình chỉ cần gọi tên chúng và sử dụng
Tuy nhiên mình cũng có thể tự viết cho mình 1 cái hàm. Luôn bắt đầu từ chữ def
những hàm do mình tự tạo: def
Cú pháp gọi hàm tự tạo
def ten hàm (tên biến) :
    nhận đối số
    Trà đối số
đã là hàm thì phải nhận được đối số và trả về đối số
"""
# Ví dụ : mình xây dựng hàm tính tổng
print("Bài 1: ")
def sum (a,b) :
    return a + b # return là để trả về kết quả, nó chưa xuất ra màn hình
# Bởi vì
sum(1,2) # nó chưa xuất ra màn hình, vì ở hàm trên mới trở về kết quả
print("Đây là kết quả: ",sum(1,2))
print(" ")
print("Bài 2: ")
# Gía trị
print("Gía trị: ", sum(1,2))
# biến
c = 1
d = 4
print("một biến: ",sum(c,d))
# Biếu thức
print("Biểu thức: ",sum(5-3,3-2))
print(" ")
print("Bài 3: ")
# biến cục bộ là biến nằm trong hàm def
def name () : # ở đây mình không truyền tham số
    ten = " Thầy Huynh"
    print(ten)
name()
""" 
print(ten) # nó sẽ báo lỗi
Vì cái biến ten chỉ nằm trong hàm def, chứ nó không nằm ở ngoài 
chính vì vậy nó sẽ báo lỗi là ten chưa được định nghĩa
"""
print(" ")
print("Bài 4: ")
def name () :
    ten = " Thầy Huynh"
    print(ten)
name()
ten = "Nguyên"
print(ten)
# nó sẽ ra nguyên
# Vì ten nằm ngoài, biến trong hàm ( cục bộ) nó không ảnh hướng đến biến ngoài . Dù tên có giống như đi chăng nữa
print(" ")
print("Bài 5: ")
def name () :
    ten = " Thầy Huynh"
    print(ten)
name()
name = "Nguyên"
print(name)
""" 
Giải thích: dù biến bên ngoài có trùng với tên hàm hay trùng với biến bên trong hàm thì 
biến bên ngoài vẫn không bị ảnh hưởng.
"""


