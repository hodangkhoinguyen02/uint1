s = "Today is nice day, so we can go anywhere"
print(s)
print(" ")
print("Câu a: Hãy đếm số ký tự trong chuỗi")
print("Số ký tự trong chuỗi: ",len(s)) # Hàm len để đếm đồ dài của chuỗi ( bao gồm khoảng trắng)
print(" ")
print("Câu b: Xuất từ vị trí -5 đến -9")
# CT: TÊN BIẾN [ví trí bắt đầu:vị trí kết thúc:bước nhảy]
print("Kết quả từ vị trí -5 đến -9 là: ",s[-5:-9])
#Kết quả : nó không ra gì hết
# Giải thích : vì số vị trí bắt đầu > số vị trí kết thúc
# Khắc phục : sửa lại số vị trí bắt đầu < số vị trí kết thúc
print(" ")
print("Câu c: Xuất tất cả với bước nhảy là 3")
#CT: TÊN BIẾN [ví trí bắt đầu:vị trí kết thúc:bước nhảy]
# Vậy muốn xuất ra tất cả vị trí thì theo cú pháp:  tên biến[:], máy nó sẽ xuất ra hết
print("Kết quả là: ",s[::3])
#Kết quả : Tainea   nonhe
# Giải thích: Today is nice day, so we can go anywhere
#Nó sẽ in Chữ T, tiếp theo nó sẽ đầu nhảy từ chữ o, nó nhảy đúng 3 chữ. Và nó sẽ lấy chữ thứ 3 là chữ a.
print(" ")
print("Câu d: tách chuỗi cho thành 2 chuỗi mới "
      "s1 = Today is nice day "
      "s2 = so we can go anywhere ")
#Cách làm:
# Bước 1 đếm từ "Today is nice day" trong s là bao nhiêu, dùng hàm len
dem1 = "Today is nice day"
print("kết quả đếm 1: ",len(dem1))
dem2 = ", so we can go anywhere"
print("kết quả đếm 2: ",len(dem2))
# Bước 2 Sau đó từ lấy chuỗi còn lại bằng cú pháp : tên biến [bắt đầu:kết thúc:bước nhảy]
s1 = s[:17]
s2 = s[17:]
print("Kết quả s1:",s1)
print("Kết quả s2:",s2)
print(" ")
print("Câu e: Hãy hợp nhất chuỗi s1 và s2 và sắp xếp theo bảng chữ cái")
#Nguồn tham khảo : https://laptrinhcanban.com/python/nhap-mon-lap-trinh-python/thao-tac-voi-chuoi-string-trong-python/join-trong-python/
# phương thức join() là nối chuỗi
# hàm sorted: là hàm sắp xếp
hopnhat = s1 + s2
print("".join(sorted(hopnhat)))
# Kết quả: ,Taaaaccddeeeeghiinnnooorsswwyyy
# Giải Thích: sorted nó sẽ ưu tiên ký tự đặc biệt ,@!#$... Sau đó là chữ cái viết hoa A->Z, tiếp theo mới tới chữ thường: a->z
print(" ")
print("Câu f: chuyển các chuỗi")
# Cú pháp viết hoa: tên biến.UPPER()
print(hopnhat.upper())



