# CÂU 1
#viết 1 chương trình python để yêu cầu người dùng nhập tên và tuổi của họ.
# Sau đó gửi lại 1 tin nhắn: cho họ biết năm họ tròn 70 tuổi.
#Luu ý: xuất kể CMD,tên file tự đặt
#Dùng hàm input để nhập :
# Cú pháp: ten biến = input = (...)
print("Câu 1: ")
print("viết 1 chương trình python để yêu cầu người dùng nhập tên và tuổi của họ. "
      "Sau đó gửi lại 1 tin nhắn: cho họ biết năm họ tròn 70 tuổi. "
      "Luu ý: xuất kể CMD,tên file tự đặt")
ten = input("Xin mời nhập tên: ")
# dùng 2 hàm lồng vào nhau để ép kiểu
# cú pháp : tên biến = int(input(..))
tuoi = int(input("Xin mời nhập năm sinh: "))
# CT tính nằm tròn 70 : Năm sinh + 70 tuổi = Năm sẽ 70 tuổi
tuoitron70 = tuoi + 70
print("Năm bạn sẽ tròn 70 tuổi là: ",tuoitron70)


print("*"*100)
#CÂU 2
#Viết 1 chương trình python yêu câu người dung nhập 1 chuỗi bất kỳ bằng tiếng anh
#Hãy xuất ra cmd : 1 chuỗi mới với thứ tự đạo ngược chuỗi ban đầu
print("Câu 2")
print("Viết 1 chương trình python yêu câu người dung nhập 1 chuỗi bất kỳ bằng tiếng anh.",
     "Hãy xuất ra cmd : 1 chuỗi mới với thứ tự đạo ngược chuỗi ban đầu" )
chuoi = input("Xin mời nhập: ")
# dùng hàm join : cÓ CHỨC năng đưa tất cả các nội dung vào chuỗi
#Cú pháp tên biến.join(đưa chuỗi vô đây)
# Dùng hàm reversed : có chức năng đảo ngược thứ tự chuỗi
# Cú pháp : tên biến = reversed(đưa chuỗi muốn đảo ngược vô đây)
print("Đây là kết quả đảo ngược: ","".join(reversed(chuoi)))


# CÂU 3
#Cho 1 cái chuỗi sau : Machine learning for Covid-19 diagnosis
#a) Hãy đếm tổng số ký tự có trong cái chuỗi
#b) kiểm tra trong chuỗi đã cho có ký tự chuỗi số hay không
#c) Chuyển chuỗi đã cho thành chuỗi in HOA
#d) thay thế chuỗi đã cho thành 1 chuỗi mới bất kỳ dược nhập từ bàn phím. Xuất đó
#xuất ra màn hình CMD : ĐẦU ĐẾN VỊ TRÍ SỐ 4
#e) loại bỏ bất kỳ khoảng trắng từ dầu dòng hoặc cuối dòng trong cái chuỗi đã cho
#f) hãy tách cái chuỗi đã cho được ngăn cách với nhau bằng dấu ','
#g)hãy kiểm tra từ py có tổn tại trong chuỗi hay không
print("*"*100)
print("Câu 3")
print(" Cho 1 cái chuỗi sau : Machine learning for Covid-19 diagnosis "
      "\na) Hãy đếm tổng số ký tự có trong cái chuỗi "
      "\nb) kiểm tra trong chuỗi đã cho có ký tự chuỗi số hay không  "
      "\nc) Chuyển chuỗi đã cho thành chuỗi in HOA "
      "\nd) thay thế chuỗi đã cho thành 1 chuỗi mới bất kỳ dược nhập từ bàn phím. Xuất đó.xuất ra màn hình CMD : ĐẦU ĐẾN VỊ TRÍ SỐ 4 "
      "\ne) loại bỏ bất kỳ khoảng trắng từ dầu dòng hoặc cuối dòng trong cái chuỗi đã cho "
      "\nf) hãy tách cái chuỗi đã cho được ngăn cách với nhau bằng dấu ',' "
      "\ng)hãy kiểm tra từ py có tổn tại trong chuỗi hay không")
chuoicho = "Machine learning for Covid-19 diagnosis"
# đếm chuỗi dùng hàm len: nó sẽ đếm chuỗi và khoảng trắng
#Cú pháp: tên biến = len(chuỗi muốn đếm)
print("Câu a: ",len(chuoicho))
# Dùng hàm islnum(): có chức năng kiểm tra chuỗi có chuỗi số hay không. Nếu có trả về True, còn không là False
#Cú pháp: tenbien.isalnum()
print("Câu b: ",chuoicho.isalnum())
#Dùng hàm upper(): có chức năng chuyển chuỗi thường thành hoa
#Cú pháp: tên biến.upper()
print("Câu c: ",chuoicho.upper())
# Dùng hàm replace: có chức năng thay thế chuỗi cũ thành chuỗi mới
#Cú pháp: tên biến mới = tên biến cũ.replace("Chuỗi cũ của tên biến","Nhập nội dung mới ở đây")
nhap = input("Xin mời nhập chuỗi mới: ")
chuoimoi = chuoicho.replace("Machine learning for Covid-19 diagnosis",nhap)
print("Câu d: Chuỗi mới là: ",chuoimoi[:4])
# Muốn xóa khaoảng trắng đầu dòng cuối dòng, thì dùng hàm strip
# CÚ PHÁP: tên biến.strip("")
# Muốn biết khoảng trắng đã loại bỏ chưa thì dùng hàm len để kiểm tra
print("Câu e: ",chuoicho.strip(" "))
print("Chuỗi chưa xóa khoảng trắng: ",len(chuoicho))
print("Chuỗi đã xóa khoảng trắng: ",len(chuoicho.strip(" ")))
# Dùng hàm split để tách chuỗi
#Cú pháp: tên biến.split():
# print("Câu f: ",chuoicho.split())
# Dùng hàm find để kiếm chuỗi: nếu có trả vị trí xuất hiện chuỗi đó, -1 là không có
#Cú pháp: tên biến.find("Chuỗi muốn tìm")
print("Câu g: ",chuoicho.find("py"))

# Câu 4
#Câu 2 hãy viết chương trình python thu thập dữ liệu người dùng từ bàn phím : tên, tuổi, quê quán, nơi đang sống
#Sau đó sử dụng phương pháp định dạng chuỗi để tạo thành 1 chuỗi có nghĩa
print("*"*100)
print("Câu 4" )
print("Câu 2 hãy viết chương trình python thu thập dữ liệu người dùng từ bàn phím : tên, tuổi, quê quán, nơi đang sống "
      "Sau đó sử dụng phương pháp định dạng chuỗi để tạo thành 1 chuỗi có nghĩa")
# dùm hàm input để nhập
ten = input("Xin mời nhập tên: ")
tuoi = input("Xin mời nhập tuổi: ")
quequan = input("Xin mời nhập quê quán: ")
# Dùng hàm format: có chức năng dành chỗ trước
#Cú pháp: "sdfdfsd{}".format(a), chữ a sẽ được đưa vô {}
print("{} rất là đẹp trai, có độ tuổi {}, đang sống tại {}".format(ten,tuoi,quequan))




